<?php
/**
 * Content anzeige
 * wird aufgerufen wenn eine Seite angezeigt werden soll
 *
 * @package WordPress
 * @subpackage CampusRadioJenaWPTemplate
 * @since CampusRadioJenaWPTemplate 1.0
 */


?>

<div class="article"><!-- START article -->

<?php

the_content();
 
?>