<?php
/**
 * Diese PHP-Seite wird aufgerufen wenn eine 
 * Artikel angezeigt werden soll.
 * 
 * @package WordPress
 * @subpackage CampusRadioJenaWPTemplate
 * @since CampusRadioJenaWPTemplate 1.0
*/


	//Seitenaufrufe mitzaehlen
	Post_Views::increment_post_views(get_the_ID());
	
	global $more;
	$more = 0;
	
	get_header(); 

?>

<div id="content_box"> <!-- BEGIN content_box -->
<div id="content_box_article"> <!-- BEGIN content_box_article -->

 
<?php 
if (have_posts()) :

	while (have_posts()) : the_post(); 
	
			get_template_part( 'content_single', 'single' );
	
	endwhile;

else : ?>

	<h2>Nichts gefunden</h2>
	<p>Es tut uns leid, aber Ihre Suchanfrage hat zu keinem Ergebnis gef&uuml;hrt.</p>

<?php endif; ?>

</div> <!-- END content_box_article -->

<?php get_sidebar(); ?>

</div> <!-- END content_box -->

<?php get_footer(); ?>