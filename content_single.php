<?php
/**
 * Content anzeige
 * wird aufgerufen wenn ein Artikel angezeigt werden soll
 *
 * @package WordPress
 * @subpackage CampusRadioJenaWPTemplate
 * @since CampusRadioJenaWPTemplate 1.0
 */

?>

		<div class="articel_meta_info">
		<?php
			if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) 
			{
			?>
		           <a href="<?php the_permalink(); ?>" title="<?php trim(the_title_attribute()); ?>" >
		             <?php the_post_thumbnail(); ?>
		           </a>
			<?php
			}
			?>
			<br/>
			<div class="tags-post">
				<?php
				the_time('j F, Y'); echo "<br/> um "; the_time('H:i');echo " Uhr";
				//echo "<br/>von: "; the_author();
				echo "<br/>" . Post_Views::get_post_views(get_the_ID());
				echo "<br/>";
				edit_post_link('Beitrag bearbeiten.',' ','');
				echo "<br/><br/>";
				comments_popup_link('Keine Kommentare bisher', '1 Kommentar bisher', '% Kommentare bisher ', 'Kommentar-Link', 'Kommentare ausgeschaltet');?>
			</div>
			<?php
			//falls das plugin installiert ist, werden die Viewer angezeigt
			if(function_exists('the_views')) { echo " | ";the_views(); }
		?>	
		</div><!-- END articel_meta_info -->


		<?php if ( is_search() ) : // Only display Excerpts for Search ?>
		<div class="entry-summary">
			<div class="articel_content">
				<?php the_excerpt(); ?>
			</div><!-- END articel_content -->
		</div><!-- .entry-summary -->
		<?php else : ?>
			
		<div class="articel_content">
		
				<header class="article_ueberschrift">
					<a href="<?php the_permalink() ?>"><?php trim(the_title())?></a>
				</header>
	
				<?php the_content(); ?>
				
				<?php if(function_exists('get_twoclick_buttons')) {get_twoclick_buttons(get_the_ID());}?>

				<footer id="author_info">
					
					<?php the_tags('<div class="tags-post">Tags: ', ", ", '</div><br/>'); ?>
					
					
					
					<div id="author_info_header">
						Author: <?php the_author(); ?>
					</div>
				
					<div id="author_info_description">
						<?php the_author_meta('description'); ?>
					</div>
				</footer> <!-- END author_info -->
					
				
				<?php comments_template('', true); ?>
			
			</div><!-- END articel_content -->
		<?php endif; ?>
