<?php
/**
 * Diese PHP-Seite wird aufgerufen wenn ein
 * Suchergebniss angezeigt werden soll.
 * 
 * @package WordPress
 * @subpackage CampusRadioJenaWPTemplate
 * @since CampusRadioJenaWPTemplate 1.0
*/
?>

<?php get_header(); ?>

<div id="content_box"> <!-- BEGIN content_box -->
<div id="content_box_article"> <!-- BEGIN content_box_article -->
<div id="content_box_info_header">Suchergebniss(e)</div>

<?php

while (have_posts()) : the_post(); 

?>

<div class="article"><!-- START article -->

		<a href="<?php the_permalink() ?>"><?php trim(the_title())?></a>

</div><!-- END article -->

<?php endwhile;  ?>
<br/>
<div id="pagenavi">
	<?php wp_pagenavi(); ?>
</div><!-- END pagenavi-->

</div> <!-- END content_box_article -->

<?php get_sidebar(); ?>

</div> <!-- END content_box -->

<?php get_footer(); ?>