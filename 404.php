<?php
/**
 * Diese PHP-Seite wird aufgefrufen wenn ein
 * 404 Error entstanden ist. (Datei nicht vorhanden)
 * 
 * @package WordPress
 * @subpackage CampusRadioJenaWPTemplate
 * @since CampusRadioJenaWPTemplate 1.0
*/

get_header(); ?>

<div id="content_box"> <!-- BEGIN content_box -->
<div id="content_box_article"> <!-- BEGIN content_box_article -->

Die angeforderte Seite ist nicht mehr verfuegbar.
 
</div> <!-- END content_box_article -->

<?php get_sidebar(); ?>

</div> <!-- END content_box -->

<?php get_footer(); ?>