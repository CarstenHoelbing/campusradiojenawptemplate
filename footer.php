<?php
/**
 * Diese PHP-Seite wird immer am ende jeder Seite aufgerufen
 * 
 * @package WordPress
 * @subpackage CampusRadioJenaWPTemplate
 * @since CampusRadioJenaWPTemplate 1.0
*/
?>

<div id="footer">

	<div id="footer_sponsoren">
		<br/>Freunde und F&ouml;rderer des <?php bloginfo('name'); ?><br/><br/>
		<a href="http://stura.inside-fhjena.de/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() ; ?>/images/footer_images/stura_eafh_logo_transparent.png" width="*" height="70" alt="Studierendenrat der EAH Jena" ></a>
		<div class="footer_space"></div>
		<a href="http://stura.uni-jena.de/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() ; ?>/images/footer_images/stura.uni-jena.logo.png" width="*" height="70" alt="Studierendenrat der FSU Jena" ></a>
		<div class="footer_space"></div>
		<a href="https://www.fsr-kowi.de/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() ; ?>/images/footer_images/fsr_kowi_jena_transparent.png" width="*" height="50" alt="FSR KoWi Jena" ></a>
		<br/>
		<div class="footer_space"></div>
		<a href="http://www.radio-okj.de/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() ; ?>/images/footer_images/radio_okj_transparent.png" width="*" height="70" alt="RADIO OKJ" ></a>
		<div class="footer_space"></div>
		<a href="http://www.eah-jena.de/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() ; ?>/images/footer_images/logo.eah.2014.png" width="*" height="70" alt="EAH Jena" ></a>
	 	<br/><br/>
	 </div>

	<div id="footer_template_info">
		Copyright &copy; <?php echo date("Y") ?> <b><a href="<?php home_url(); ?>"><?php bloginfo('name'); ?></a>. All rights reserved.<br/>
		<?php echo get_template() ?> Theme by <a href="http://www.hoelbing.net/">Carsten H&ouml;lbing</a></b>.<br/>
	</div>
 
	<div id="footer_content">
		Redaktion Campusradio Jena -- Ernst-Abbe-Hochschule Jena -- Carl-Zeiss-Promenade 2 -- 07745 Jena<br/>
		Tel. +49 3641 205-796 -- E-Mail: redaktion@campusradio-jena.de <br/>			
		<a href="<?php bloginfo('rss2_url'); ?>" title="RSS">RSS</a>  |
		<a href="<?php bloginfo('url'); ?>/?page_id=522" title="Impressum">Impressum</a> | 
		<a href="<?php bloginfo('url'); ?>/?page_id=12857" title="Datenschutz">Datenschutz</a> | 
		<a href="<?php bloginfo('url'); ?>/?page_id=31" title="Kontakt">Kontakt</a> | 
		<a href="<?php echo wp_login_url(); ?>" title="Login">Login</a>
	</div>

</div><!-- END  footer-->

</div><!-- END  wrapper-->

<?php 
/**
 * Social Bar
 * show left, center (hor), fb img, 
 */
 ?>
<div id="sidebar_socialLinks">

<a href="https://www.facebook.com/campusradiojena" target="_blank" >
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/fb_icon.small.png" alt="hier geht es zum Facebook Auftritt des Campusradio-Jena" title="hier geht es zum Facebook Auftritt des Campusradio-Jena" width="25" height="25"/>	
</a>

</div><!-- end sidebar_socialLinks -->

<?php 
/**
 * Code from Piwik
 * (Hardcode)
 */
 ?>

<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(["setDoNotTrack", true]);
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//stats.stura.uni-jena.de/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', 12]);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<noscript><p><img src="https://stats.stura.uni-jena.de/piwik.php?idsite=12" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->


<?php wp_footer(); ?>

</body>
</html>