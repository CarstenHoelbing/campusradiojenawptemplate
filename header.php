<?php
/**
 * Diese PHP-Seite wird aufgerufen wenn der 
 * Header angezeigt wird.
 * 
 * @package WordPress
 * @subpackage CampusRadioJenaWPTemplate
 * @since CampusRadioJenaWPTemplate 1.0
*/

?>

<!--[if IE 7]>
<html class="ie ie7" lang="de-DE">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="de-DE">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="de-DE">
<!--<![endif]-->
<head>

<title><?php 
	bloginfo( 'name' ); 
	wp_title();
	global $paged ;
	$page_max = $wp_query->max_num_pages;
	if ( $paged >= 2 || $page >= 2 )
	{
		echo ' | ' . sprintf( 'Seite %s von %s ', $paged, $page_max );
	}
?></title> 

<link rel="profile" href="http://gmpg.org/xfn/11">

<?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>

<meta name="description" content="<?php if ( is_single() ) {
        single_post_title('', true); 
    } else {
        bloginfo('name');
    }
    ?>"
>
    
<meta name="keywords" lang="de" content="Campusradio Jena, Campusradio, Jena, Radio, OKJ, Campusradio,Thüringen, aktuelle Themen, Beiträge, Eule, Eulenfreunde">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="content-language" content="<?php bloginfo('language'); ?>">
<meta http-equiv="pragma" content="no-cache">

<link type="image/x-icon" rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico">

<link type="application/atom+xml" rel="alternate" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>">

<link rel="pingback" href="<?php bloginfo ('pingback_url'); ?>">

<?php 
	wp_head();
?>

</head> 

<body <?php body_class($class); ?>>

<div id="wrapper">
	
    <div id="page_header">
		<div id="page_header_logoEule">
			<a href="<?php echo home_url(); ?>">
				<img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>"  alt="Campusradio Jena Logo" />
			</a>			
		</div>
		<div id="page_header_right">
			<div class="page_header_right_img">
				<a href="<?php home_url(); ?>/interaktiv/playlist/">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/header_small_img/playlist.png" alt="Playlist" />
				</a>
				<br/>
				<a href="<?php home_url(); ?>/interaktiv/playlist/">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/header_small_img/playlist_sc.png" alt="Playlist" />
				</a>
			</div>
			
			<div class="page_header_right_img">
<!-- 				<a title="Player starten" onclick="window.open('<?php home_url(); ?>/webplayer/webplayer.html','player','width=380,height=170'); return false; resizable=no; location=no; status=no" href="<?php home_url(); ?>/webplayer/webplayer.html">
 -->
				<a href="<?php home_url(); ?>/interaktiv/webplayer/">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/header_small_img/hoeren.png" alt="Hoeren" />
				</a>
				<br/>
<!--				<a title="Player starten" onclick="window.open('<?php home_url(); ?>/webplayer/webplayer.html','player','width=380,height=170'); return false; resizable=no; location=no; status=no" href="<?php home_url(); ?>/webplayer/webplayer.html">
-->
				<a href="<?php home_url(); ?>/interaktiv/webplayer/">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/header_small_img/hoeren_sc.png" alt="Hoeren" />
				</a>
			</div>
			
			<div class="page_header_right_img">
				<a href="<?php home_url(); ?>/interaktiv/studiocam/">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/header_small_img/cam.png" alt="StudiCam" />
				</a>
				<br/>
				<a href="<?php home_url(); ?>/interaktiv/studiocam/">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/header_small_img/cam_sc.png" alt="StudiCam" />
				</a>
			</div>
			
		</div> <!-- END page_header_right -->

	</div><!-- END page_header -->
	
	<div id="nav">
		<div id="menus">
			<ul><li<?php if (is_home()) echo ' class="current_page_item"'; ?>><a href="<?php echo home_url(); ?>">Startseite</a></li></ul>
			<?php wp_nav_menu( array( 'container' => 'none', 'theme_location' => 'primary' ) ); ?>
		</div>
	</div><!-- END nav -->
