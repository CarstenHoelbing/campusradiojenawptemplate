<?php
/**
 * Content anzeige
 * wird auf der Startseite und folgend aufgerufen
 *
 * @package WordPress
 * @subpackage CampusRadioJenaWPTemplate
 * @since CampusRadioJenaWPTemplate 1.0
 */

?>
	<div class="article"><!-- START article -->
	
	 <div class="articel_meta_info">
	<?php
		if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) 
		{
		?>
	                       <a href="<?php the_permalink(); ?>" title="<?php trim(the_title_attribute()); ?>" >
	                         <?php the_post_thumbnail(); ?>
	                       </a>
		<?php
		}
	
	
		echo "<br/>";
		echo '<div class="post-metainfo">';
		the_time('j F, Y'); echo "<br/> um "; the_time('H:i');echo " Uhr";
		echo "<br/>von: "; the_author();
		echo "<br/>";
		edit_post_link('Beitrag bearbeiten.',' ','');
		echo "<br/><br/>";
		comments_popup_link('Keine Kommentare bisher', '1 Kommentar bisher', '% Kommentare bisher ', 'Kommentar-Link', 'Kommentare ausgeschaltet');
		echo '</div>';
		//falls das plugin installiert ist, werden die Viewer angezeigt
		if(function_exists('the_views')) { echo " | ";the_views(); }
	?>	
	</div><!-- END articel_meta_info -->
	
	<div class="articel_content">
	
		<div class="article_ueberschrift">
			<a href="<?php the_permalink() ?>"><?php trim(the_title())?></a>
		</div>
			<?php 
				//global $more;
				//$more = 0;	 
				the_content('Weiterlesen ...');
				the_tags('<div class="tags-post">Tags: ', ", ", '</div>');
			?>
	
	</div><!-- END articel_content -->
	
	</div><!-- END article -->