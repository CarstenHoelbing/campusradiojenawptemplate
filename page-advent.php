<?php
/**
 * (neu) geschrieben am 2015-11-26
 * von Carsten Hoelbing
 *
 * Template Name: Adventskalender
 *
 */

$year = 2015;
?>

<!--[if IE 7]>
<html class="ie ie7" lang="de-DE">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="de-DE">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="de-DE">
	<!--<![endif]-->
	<head>

		<title>Campusradio Jena - Adventskalender <?php $year ?> </title>

		<meta name="description" content="dies ist der Adventskalender <?php $year ?> vom Campusradio-Jena, wir wünscht allen eine besinnliche Adventszeit">
		<meta name="keywords" content="Campusradio Adventskalender, Campusradio Jena Adventskalender, Campusradio, Campusradio Jena, Jena, Adventskalender">
		<meta name="copyright" content="Campusradio Adventskalender">
		<meta name="robots" content="index,follow">

		<meta http-equiv="Language" content="de">
		<meta http-equiv="content-language" content="de-DE">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

		<meta http-equiv="Content-Style-Type" content="text/css">

		<link type="image/x-icon" rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/advent/favicon.ico">
		<link type="text/css" rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/adventskalende.index.style.css" media="screen">

		<!-- <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js"></script> -->
		<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/javascript/adventskalender2.js"></script>


		<?php
		wp_head();
		?>

	</head>

	<body <?php body_class($class); ?>>
		<?php
		/* schnee abschalten
		<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/javascript/jquery.snow.min.1.0.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {

				$.fn.snow();

			});
			*/?>
		</script>

		<div id="wrapperAdvent">
			
		<div id="header">
		<?php
		$TageLang = array('1' => 'Montag','2' =>'Dienstag','3' =>'Mittwoch', '4' =>'Donnerstag', '5' =>'Freitag', '6' =>'Samstag', '0' =>'Sonntag');
		$MonateLang = array('01' =>'Januar', '02' =>'Februar', '03' =>'März', '04' =>'April', '05' =>'Mai', '06' =>'Juni', '07' =>'Juli', '08' =>'August', '09' =>'September', '10' =>'Oktober', '11' =>'November', '12' =>'Dezember');
		$html_output = "";
		$time_now = mktime(date("H"), date("i"), date("s"), date("n"), date("j"));
		$time_adv = mktime("0", "0", "0", "12", "25");
		$time_div = $time_adv - $time_now;
		$time_div = $time_div / 60 / 60 / 24;
		$html_output = "heute ist " . $TageLang[date("w")] . " der " . date("d") . ". " . $MonateLang[date("m")] . " " . date("Y");

		if ($month == '12') {
			//$day = date("j");
			if ($day <= 22) {
				$html_output .= " - noch " . sprintf("%d", $time_div + 1) . " Tage bis Weihnachten ";
				$html_output .= "- Sucht das Fensterchen f&uuml;r heute!";
			} else if ($day == 23) {
				$html_output .= "Morgen ist Heiligabend!";
			} else if ($day == 24) {
				$html_output .= "Heut ist Heiligabend!";
			} else if (($day >= 25) and ($day <= 30)) {
				$html_output .= "Frohe Weihnachten - Alle Fensterchen d&uuml;rfen ge&ouml;ffnet werden";
			} else {
				$html_output .= "Weihnachten ist vorbei!";
			}
		} else {
			$html_output .= " - noch " . sprintf("%d", $time_div) . " Tage bis Weihnachten";
		}

		echo $html_output;
		?>

		</div><!-- END  header-->
		
		<div id="contentAdvent">

		<div id="contentAdvent_info">
		<h1 id="contentAdvent_info_header">Campusradio Adventskalender</h1>
		<br/>
		<h2 id="contentAdvent_info_header_text">Das Campusradio Jena w&uuml;nscht allen eine besinnliche Adventszeit</h2>
		</div>

		<div id="content_tree">
		<map name="Beispiel">
		<area alt="01" onMouseOver="popLayer(1)" onMouseOut="hideLayer()" shape="circle" coords="148, 425, 15" href="<?php echo $page_name; ?>?day_nr=01">
		<area alt="02" onMouseOver="popLayer(2)" onMouseOut="hideLayer()" shape="circle" coords="398, 314, 15" href="<?php echo $page_name; ?>?day_nr=02">
		<area alt="03" onMouseOver="popLayer(3)" onMouseOut="hideLayer()" shape="circle" coords="472, 579, 15" href="<?php echo $page_name; ?>?day_nr=03">
		<area alt="04" onMouseOver="popLayer(4)" onMouseOut="hideLayer()" shape="circle" coords="272, 78, 15" href="<?php echo $page_name; ?>?day_nr=04">
		<area alt="05" onMouseOver="popLayer(5)" onMouseOut="hideLayer()" shape="circle" coords="174, 526, 15" href="<?php echo $page_name; ?>?day_nr=05">
		<area alt="06" onMouseOver="popLayer(6)" onMouseOut="hideLayer()" shape="circle" coords="276, 215, 15" href="<?php echo $page_name; ?>?day_nr=06">
		<area alt="07" onMouseOver="popLayer(7)" onMouseOut="hideLayer()" shape="circle" coords="290, 482, 15" href="<?php echo $page_name; ?>?day_nr=07">
		<area alt="08" onMouseOver="popLayer(8)" onMouseOut="hideLayer()" shape="circle" coords="56, 580, 15" href="<?php echo $page_name; ?>?day_nr=08">
		<area alt="09" onMouseOver="popLayer(9)" onMouseOut="hideLayer()" shape="circle" coords="323, 326, 15" href="<?php echo $page_name; ?>?day_nr=09">
		<area alt="10" onMouseOver="popLayer(10)" onMouseOut="hideLayer()" shape="circle" coords="467, 463, 15" href="<?php echo $page_name; ?>?day_nr=10">
		<area alt="11" onMouseOver="popLayer(11)" onMouseOut="hideLayer()" shape="circle" coords="207, 165, 15" href="<?php echo $page_name; ?>?day_nr=11">
		<area alt="12" onMouseOver="popLayer(12)" onMouseOut="hideLayer()" shape="circle" coords="137, 324, 15" href="<?php echo $page_name; ?>?day_nr=12">
		<area alt="13" onMouseOver="popLayer(13)" onMouseOut="hideLayer()" shape="circle" coords="245, 569, 15" href="<?php echo $page_name; ?>?day_nr=13">
		<area alt="14" onMouseOver="popLayer(14)" onMouseOut="hideLayer()" shape="circle" coords="392, 384, 15" href="<?php echo $page_name; ?>?day_nr=14">
		<area alt="15" onMouseOver="popLayer(15)" onMouseOut="hideLayer()" shape="circle" coords="306, 142, 15" href="<?php echo $page_name; ?>?day_nr=15">
		<area alt="16" onMouseOver="popLayer(16)" onMouseOut="hideLayer()" shape="circle" coords="416, 520, 15" href="<?php echo $page_name; ?>?day_nr=16">
		<area alt="17" onMouseOver="popLayer(17)" onMouseOut="hideLayer()" shape="circle" coords="180, 231, 15" href="<?php echo $page_name; ?>?day_nr=17">
		<area alt="18" onMouseOver="popLayer(18)" onMouseOut="hideLayer()" shape="circle" coords="76, 464, 15" href="<?php echo $page_name; ?>?day_nr=18">
		<area alt="19" onMouseOver="popLayer(19)" onMouseOut="hideLayer()" shape="circle" coords="211, 444, 15" href="<?php echo $page_name; ?>?day_nr=19">
		<area alt="20" onMouseOver="popLayer(20)" onMouseOut="hideLayer()" shape="circle" coords="328, 541, 15" href="<?php echo $page_name; ?>?day_nr=20">
		<area alt="21" onMouseOver="popLayer(21)" onMouseOut="hideLayer()" shape="circle" coords="249, 370, 15" href="<?php echo $page_name; ?>?day_nr=21">
		<area alt="22" onMouseOver="popLayer(22)" onMouseOut="hideLayer()" shape="circle" coords="369, 214, 15" href="<?php echo $page_name; ?>?day_nr=22">
		<area alt="23" onMouseOver="popLayer(23)" onMouseOut="hideLayer()" shape="circle" coords="358, 444, 15" href="<?php echo $page_name; ?>?day_nr=23">
		<area alt="24" onMouseOver="popLayer(24)" onMouseOut="hideLayer()" shape="circle" coords="228, 297, 15" href="<?php echo $page_name; ?>?day_nr=24">
		</map>

		<img usemap="#Beispiel" src="<?php echo get_stylesheet_directory_uri(); ?>/images/advent/tree24-01.gif" width="550" height="660" border="0" alt="Baum">
		<br>
		</div><!-- END  content_tree-->

		</div><!-- END  content-->

		<?php

		if (isset($_GET['day_nr']) AND ($_GET['day_nr'] >= 1) AND ($_GET['day_nr'] <= 24)) {
			
			//$day_nr = get_post($_GET['day_nr']);
			$day_nr = $_GET['day_nr'];
			$pageURL = "https://" . $_SERVER['SERVER_NAME'] . "/adventskalender/";
			
			//cheater methode
			if (isset($_GET['abc'])) {
				//$day = $_GET['day_nr'];
				$day_server = $day_nr;
			} else {
				$day_server = date('d');
				$month = date('m');
			}

			if (($day_server >= $day_nr) and ($month == '12')) {

				$post_id = get_option('crj_advent_link_tag' . $_GET['day_nr']);
				$queried_post = get_post($post_id);

				echo '<article id="post-'. $post_id .'" class="boxAdvent">'. "\n";

				echo '<header class="advent-post-header">'. "\n";

				//if (function_exists('has_post_thumbnail') && has_post_thumbnail($queried_post ->id)) {
				//	echo '<?php the_post_thumbnail($queried_post ->id); ? >';
				//}

				echo '  <h3>Türchen Nummer: '. $day_nr.'</h3>'. "\n";
				echo '</header><!-- .entry-header -->'. "\n";

				// content/textausgabe
				echo '<div class="advent-post-content">' . "\n";
				$content = $queried_post -> post_content;
				$content = apply_filters('the_content', $content);
				$content = str_replace(']]>', ']]&gt;', $content);
				echo $content . "\n";
				echo '</div>' . "\n";

				echo '<footer class="advent-post-footer">';
				echo  '<a href="' . $pageURL . '">Fenster schlie&szlig;en!</a>';
				echo '</footer>'. "\n";

				echo '</article><!-- #post -->'. "\n";


			} else if ($month != '12') {
				
				echo '<article id="boxAdvent_error" class="boxAdvent_error">'. "\n";

				echo '<header class="advent-post-header">'. "\n";
				echo '  <h3>ja ist denn schon Dezember oder was?</h3>'. "\n";
				echo '</header><!-- .entry-header -->'. "\n";

				// content/textausgabe
				echo '<div class="advent-post-content">' . "\n";
				echo '<img src="/wp-content/themes/CampusRadioJenaWPTemplate/images/advent/w-mann01-ani.gif" width="65" height="103" border="0" alt="bad" align="middle">';
				echo '<br/>';
				echo 'Mogeln gilt nicht.';
				echo '</div>' . "\n";

				echo '<footer class="advent-post-footer">';
				echo  '<a href="' . $pageURL . '">Fenster schlie&szlig;en!</a>';
				echo '</footer>'. "\n";

				echo '</article><!-- #post -->'. "\n";

			} else {
				echo '<article id="boxAdvent_error" class="boxAdvent_error">'. "\n";

				echo '<header class="advent-post-header">'. "\n";
				echo '  <h3>Böse Kinder kriegen nix!</h3>'. "\n";
				echo '</header><!-- .entry-header -->'. "\n";

				// content/textausgabe
				echo '<div class="advent-post-content">' . "\n";
				echo '<img src="/wp-content/themes/CampusRadioJenaWPTemplate/images/advent/w-mann01-ani.gif" width="65" height="103" border="0" alt="bad" align="middle">';
				echo '<br/>';
				echo 'Mogeln gilt nicht, nur die Türchen für den heutigen oder einen früheren Tag öffnen. ';
				echo '</div>' . "\n";

				echo '<footer class="advent-post-footer">';
				echo  '<a href="' . $pageURL . '">Fenster schlie&szlig;en!</a>';
				echo '</footer>'. "\n";

				echo '</article><!-- #post -->'. "\n";
			}
		}
		?>


		</div><!-- END  wrapper-->

		<!-- Piwik -->
		<script type="text/javascript">
			var _paq = _paq || [];
			_paq.push(["trackPageView"]);
			_paq.push(["enableLinkTracking"]);

			(function() {
				var u = (("https:" == document.location.protocol) ? "https" : "http") + "://stats.stura.uni-jena.de/";
				_paq.push(["setTrackerUrl", u + "piwik.php"]);
				_paq.push(["setSiteId", "12"]);
				var d = document, g = d.createElement("script"), s = d.getElementsByTagName("script")[0];
				g.type = "text/javascript";
				g.defer = true;
				g.async = true;
				g.src = u + "piwik.js";
				s.parentNode.insertBefore(g, s);
			})();
		</script>
		<!-- End Piwik Code -->

		<?php wp_footer(); ?>

	</body>

</html>