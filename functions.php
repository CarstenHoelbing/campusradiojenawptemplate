<?php
/*
 * der Code der die Klasse "Post_Views"
 * bassiert auf: http://www.smart-webentwicklung.de/2012/08/wordpress-artikel-views-anzeigen-ohne-plugin/
 * 
 * @package WordPress
 * @subpackage CampusRadioJenaWPTemplate
 * @since CampusRadioJenaWPTemplate 1.0
*/

function campusradiotemplate_setup()
{
	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );
	
	/*
	 * This theme uses a custom image size for featured images, displayed on
	 * "standard" posts and pages.
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 145, 150, true ); // 145 pixels wide by 150 pixels tall, resize mode

	// Switches default core markup for search form, comment form, and comments
	// to output valid HTML5.
	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );

	/*
	 * This theme supports all available post formats by default.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'audio', 'chat', 'gallery', 'image', 'link', 'quote', 'status', 'video'
	) );

	$args = array(
	    'flex-height'            => false, /* Flexible Höhe (true|false) */
	    'height'                 => 141, /* Höhe des Header */
	    'flex-width'             => false, /* Flexible Weite (true|false) */
	    'width'                  => 300, /* Weite des Headers */
	    'default-image'          => get_template_directory_uri() . '/images/logoEule.png', /* Standard-Bild des Headers */
	    'random-default'         => false, /* Zufallsdurchlauf, wenn es mehrere Bilder gibt */
	    'header-text'            => false, /* Header Text aktivieren oder abschalten */
	    'uploads'                => true /* User-Uploads erlauben */
	);
	 
	add_theme_support( 'custom-header', $args );
	register_default_headers( array(
	    'normal' => array(
	        'url' => '%s/images/logoEule.png', /* URL zum Bild */
	        'thumbnail_url' => '%s/images/logoEule.png', /* URL zum Thumbnail (130px*66px) */
	        'description' =>  'Logo' /* Bildbeschreibung */
	    ),
	    'weihnachten' => array(
	        'url' => '%s/images/logoEule.weihnachten.png', /* URL zum Bild */
	        'thumbnail_url' => '%s/images/logoEule.weihnachten.png', /* URL zum Thumbnail (130px*66px) */
	        'description' => 'Logo' /* Bildbeschreibung */
	    )
	));

	//remove Version number
	remove_action('wp_head', 'wp_generator');  

}
add_action( 'after_setup_theme', 'campusradiotemplate_setup' );


// WP nav menu
if (function_exists('wp_nav_menu')) {
	register_nav_menus(array('primary' => 'nav_menus_header'));
}

function campusradiotemplate_widgets_init() 
{
	register_sidebar(
		array(
			'name' => 'sidebar_box_content_banner',
			'id' => 'sidebar-1',
			'description' => 'Diese Sidebar ist nur fuer die Events Banner gedacht',
			'before_title' => '',
			'after_title' => '',
			'before_widget' => '',
			'after_widget' => '',
		)
	);

	register_sidebar(
		array(
			'name' => 'sidebar_box_content',
			'id' => 'sidebar-2',
			'description' => 'sidebar_box_content',
			'before_title' => '',
			'after_title' => '',
			'before_widget' => '',
			'after_widget' => '',
		)
	);

}
add_action( 'widgets_init', 'campusradiotemplate_widgets_init' );

// anpassen der eingabe kommentarfelder
function campusradiotemplate_comment_form_filter($fields) {
	$fields['author'] = '<p class="comment-form-author">' . 
	                    ' <br/>' .
	                    ' <input id="author" name="author" type="text" class="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' />' .
	                    ' <label for="author">' . 'Name*' . '</label> ' . ( $req ? '<span class="required">*</span>' : '' ) .
	                    '</p>';
	                     
	$fields['email'] = '<p class="comment-form-email">'.
	                    ' <br/>' .
	                    ' <input id="email" name="email" type="text" class="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' />' .
						' <label for="email">' . 'Email*'  . '</label> ' . ( $req ? '<span class="required">*</span>' : '' ) .
	                    '</p>';
	                     
	$fields['url'] = '<p class="comment-form-url">'.
	                    ' <br/>' .
	                    ' <input id="url" name="url" type="text" class="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" />' .
						' <label for="url">' . 'Website'  . '</label>' .
	                    '</p>';		
	return $fields;
}
add_filter('comment_form_default_fields','campusradiotemplate_comment_form_filter');

// libs die nochmal mit geladen werden sollen
// Bugfix: Workaround
function insert_wp_head_stuff(){
?>
<script type='text/javascript' src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<?php
	}

	add_action('wp_head', 'insert_wp_head_stuff');

	// hier werden externe JavaScript Bibliotheken und dazugehoerige CSS Dateien geladen
	function load_js_libs() {
	wp_enqueue_script('jquery-ui-core');
	wp_enqueue_script('jquery-ui-tabs');
	}
	//add_action('wp_enqueue_scripts', 'load_js_libs');http://www.smart-webentwicklung.de/2012/08/wordpress-artikel-views-anzeigen-ohne-plugin/
	add_action('init', 'load_js_libs');

	add_filter('option_ping_sites','privacy_ping_filter');
	//add_filter('option_blog_charset','_wp_specialchars');

	function template_styles() {
	wp_register_style( 'template_css', get_template_directory_uri(). '/css/general.css' );
	wp_enqueue_style( 'template_css' );
	}
	add_action('wp_enqueue_scripts', 'template_styles');

	/**
	*  ADMIN Page
	* d.h. gilt nur wenn User in Admin Bereich ist
	*/
	if ( is_admin() )
	{
	wp_enqueue_style('CampusRadioJenaAdvents', get_bloginfo('stylesheet_directory')."/css/layout_admin.css");

	// Adminmenu Optionen erweitern
	function ShowSendungsDate_add_menu()
	{

	// Hauptpunkt anlegen (im Admin Bereich)
	add_menu_page('Adventskalender', 'Adventskalender', 8, __FILE__, 'ShowAdminAdvent_page');

	// wenn Variablen noch nicht vorhanden sind dann anlegen
	if (get_option('crj_advent_link_tag01') != "")
	{
	add_option( 'crj_advent_link_tag01', '' );
	add_option( 'crj_advent_link_tag02', '' );
	add_option( 'crj_advent_link_tag03', '' );
	add_option( 'crj_advent_link_tag04', '' );
	add_option( 'crj_advent_link_tag05', '' );
	add_option( 'crj_advent_link_tag06', '' );
	add_option( 'crj_advent_link_tag07', '' );
	add_option( 'crj_advent_link_tag08', '' );
	add_option( 'crj_advent_link_tag09', '' );
	add_option( 'crj_advent_link_tag10', '' );
	add_option( 'crj_advent_link_tag11', '' );
	add_option( 'crj_advent_link_tag12', '' );
	add_option( 'crj_advent_link_tag13', '' );
	add_option( 'crj_advent_link_tag14', '' );
	add_option( 'crj_advent_link_tag15', '' );
	add_option( 'crj_advent_link_tag16', '' );
	add_option( 'crj_advent_link_tag17', '' );
	add_option( 'crj_advent_link_tag18', '' );
	add_option( 'crj_advent_link_tag19', '' );
	add_option( 'crj_advent_link_tag20', '' );
	add_option( 'crj_advent_link_tag21', '' );
	add_option( 'crj_advent_link_tag22', '' );
	add_option( 'crj_advent_link_tag23', '' );
	add_option( 'crj_advent_link_tag24', '' );
	}
	}

	// Menu Punkt im Admin Bereich anlegen
	add_action( 'admin_menu', 'ShowSendungsDate_add_menu' );

	function ShowAdminAdvent_page()
	{
	if (isset($_POST['submit_save']))
	{
	update_option( 'crj_advent_link_tag01', htmlspecialchars(strip_tags(stripslashes( $_POST['crj_advent_link_tag01'] ))) );
	update_option( 'crj_advent_link_tag02', htmlspecialchars(strip_tags(stripslashes( $_POST['crj_advent_link_tag02'] ))) );
	update_option( 'crj_advent_link_tag03', htmlspecialchars(strip_tags(stripslashes( $_POST['crj_advent_link_tag03'] ))) );
	update_option( 'crj_advent_link_tag04', htmlspecialchars(strip_tags(stripslashes( $_POST['crj_advent_link_tag04'] ))) );
	update_option( 'crj_advent_link_tag05', htmlspecialchars(strip_tags(stripslashes( $_POST['crj_advent_link_tag05'] ))) );
	update_option( 'crj_advent_link_tag06', htmlspecialchars(strip_tags(stripslashes( $_POST['crj_advent_link_tag06'] ))) );
	update_option( 'crj_advent_link_tag07', htmlspecialchars(strip_tags(stripslashes( $_POST['crj_advent_link_tag07'] ))) );
	update_option( 'crj_advent_link_tag08', htmlspecialchars(strip_tags(stripslashes( $_POST['crj_advent_link_tag08'] ))) );
	update_option( 'crj_advent_link_tag09', htmlspecialchars(strip_tags(stripslashes( $_POST['crj_advent_link_tag09'] ))) );
	update_option( 'crj_advent_link_tag10', htmlspecialchars(strip_tags(stripslashes( $_POST['crj_advent_link_tag10'] ))) );
	update_option( 'crj_advent_link_tag11', htmlspecialchars(strip_tags(stripslashes( $_POST['crj_advent_link_tag11'] ))) );
	update_option( 'crj_advent_link_tag12', htmlspecialchars(strip_tags(stripslashes( $_POST['crj_advent_link_tag12'] ))) );
	update_option( 'crj_advent_link_tag13', htmlspecialchars(strip_tags(stripslashes( $_POST['crj_advent_link_tag13'] ))) );
	update_option( 'crj_advent_link_tag14', htmlspecialchars(strip_tags(stripslashes( $_POST['crj_advent_link_tag14'] ))) );
	update_option( 'crj_advent_link_tag15', htmlspecialchars(strip_tags(stripslashes( $_POST['crj_advent_link_tag15'] ))) );
	update_option( 'crj_advent_link_tag16', htmlspecialchars(strip_tags(stripslashes( $_POST['crj_advent_link_tag16'] ))) );
	update_option( 'crj_advent_link_tag17', htmlspecialchars(strip_tags(stripslashes( $_POST['crj_advent_link_tag17'] ))) );
	update_option( 'crj_advent_link_tag18', htmlspecialchars(strip_tags(stripslashes( $_POST['crj_advent_link_tag18'] ))) );
	update_option( 'crj_advent_link_tag19', htmlspecialchars(strip_tags(stripslashes( $_POST['crj_advent_link_tag19'] ))) );
	update_option( 'crj_advent_link_tag20', htmlspecialchars(strip_tags(stripslashes( $_POST['crj_advent_link_tag20'] ))) );
	update_option( 'crj_advent_link_tag21', htmlspecialchars(strip_tags(stripslashes( $_POST['crj_advent_link_tag21'] ))) );
	update_option( 'crj_advent_link_tag22', htmlspecialchars(strip_tags(stripslashes( $_POST['crj_advent_link_tag22'] ))) );
	update_option( 'crj_advent_link_tag23', htmlspecialchars(strip_tags(stripslashes( $_POST['crj_advent_link_tag23'] ))) );
	update_option( 'crj_advent_link_tag24', htmlspecialchars(strip_tags(stripslashes( $_POST['crj_advent_link_tag24'] ))) );

	echo "aktualisierte Daten wurden abgespeichert:";
	}

	$html_output = '<div id="Plugin_Wrapper">';
	$html_output .= '<div id="Plugin_Wrapper_Content">';

	$html_output .= "\n".'<!--start Themplate_Option_Page_Show-->'."\n";

	$html_output .= '<form name="form1" method="post" action="#">';

	$html_output .= '<div class="header_title">Adventskalender</div>'."\n";

	$html_output .= 'in URL, die Zahl auslesen und eingeben https://www.campusradio-jena.de/wp-admin/post.php?post=<br />';
	$html_output .= 'Link: <a href="https://www.campusradio-jena.de/adventskalender">https://www.campusradio-jena.de/adventskalender</a>';

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text_title">';
	$html_output .=         'Tag 1 Beitragslink';
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .=         '<input name="crj_advent_link_tag01" size="50" maxlength="50" type="text" value="'.get_option('crj_advent_link_tag01').'" />';
	$html_output .= '</div>';
	$html_output .= '</div>'.'<!-- END infotext -->'."\n";
	$html_output .= '<br/>'."\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text_title">';
	$html_output .=         'Tag 2 Beitragslink';
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .=         '<input name="crj_advent_link_tag02" size="50" maxlength="50" type="text" value="'.get_option('crj_advent_link_tag02').'" />';
	$html_output .= '</div>';
	$html_output .= '</div>'.'<!-- END infotext -->'."\n";
	$html_output .= '<br/>'."\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text_title">';
	$html_output .=         'Tag 3 Beitragslink';
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .=         '<input name="crj_advent_link_tag03" size="50" maxlength="50" type="text" value="'.get_option('crj_advent_link_tag03').'" />';
	$html_output .= '</div>';
	$html_output .= '</div>'.'<!-- END infotext -->'."\n";
	$html_output .= '<br/>'."\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text_title">';
	$html_output .=         'Tag 4 Beitragslink';
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .=         '<input name="crj_advent_link_tag04" size="50" maxlength="50" type="text" value="'.get_option('crj_advent_link_tag04').'" />';
	$html_output .= '</div>';
	$html_output .= '</div>'.'<!-- END infotext -->'."\n";
	$html_output .= '<br/>'."\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text_title">';
	$html_output .=         'Tag 5 Beitragslink';
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .=         '<input name="crj_advent_link_tag05" size="50" maxlength="50" type="text" value="'.get_option('crj_advent_link_tag05').'" />';
	$html_output .= '</div>';
	$html_output .= '</div>'.'<!-- END infotext -->'."\n";
	$html_output .= '<br/>'."\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text_title">';
	$html_output .=         'Tag 6 Beitragslink';
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .=         '<input name="crj_advent_link_tag06" size="50" maxlength="50" type="text" value="'.get_option('crj_advent_link_tag06').'" />';
	$html_output .= '</div>';
	$html_output .= '</div>'.'<!-- END infotext -->'."\n";
	$html_output .= '<br/>'."\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text_title">';
	$html_output .=         'Tag 7 Beitragslink';
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .=         '<input name="crj_advent_link_tag07" size="50" maxlength="50" type="text" value="'.get_option('crj_advent_link_tag07').'" />';
	$html_output .= '</div>';
	$html_output .= '</div>'.'<!-- END infotext -->'."\n";
	$html_output .= '<br/>'."\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text_title">';
	$html_output .=         'Tag 8 Beitragslink';
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .=         '<input name="crj_advent_link_tag08" size="50" maxlength="50" type="text" value="'.get_option('crj_advent_link_tag08').'" />';
	$html_output .= '</div>';
	$html_output .= '</div>'.'<!-- END infotext -->'."\n";
	$html_output .= '<br/>'."\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text_title">';
	$html_output .=         'Tag 9 Beitragslink';
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .=         '<input name="crj_advent_link_tag09" size="50" maxlength="50" type="text" value="'.get_option('crj_advent_link_tag09').'" />';
	$html_output .= '</div>';
	$html_output .= '</div>'.'<!-- END infotext -->'."\n";
	$html_output .= '<br/>'."\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text_title">';
	$html_output .=         'Tag 10 Beitragslink';
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .=         '<input name="crj_advent_link_tag10" size="50" maxlength="50" type="text" value="'.get_option('crj_advent_link_tag10').'" />';
	$html_output .= '</div>';
	$html_output .= '</div>'.'<!-- END infotext -->'."\n";
	$html_output .= '<br/>'."\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text_title">';
	$html_output .=         'Tag 11 Beitragslink';
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .=         '<input name="crj_advent_link_tag11" size="50" maxlength="50" type="text" value="'.get_option('crj_advent_link_tag11').'" />';
	$html_output .= '</div>';
	$html_output .= '</div>'.'<!-- END infotext -->'."\n";
	$html_output .= '<br/>'."\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text_title">';
	$html_output .=         'Tag 12 Beitragslink';
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .=         '<input name="crj_advent_link_tag12" size="50" maxlength="50" type="text" value="'.get_option('crj_advent_link_tag12').'" />';
	$html_output .= '</div>';
	$html_output .= '</div>'.'<!-- END infotext -->'."\n";
	$html_output .= '<br/>'."\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text_title">';
	$html_output .=         'Tag 13 Beitragslink';
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .=         '<input name="crj_advent_link_tag13" size="50" maxlength="50" type="text" value="'.get_option('crj_advent_link_tag13').'" />';
	$html_output .= '</div>';
	$html_output .= '</div>'.'<!-- END infotext -->'."\n";
	$html_output .= '<br/>'."\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text_title">';
	$html_output .=         'Tag 14 Beitragslink';
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .=         '<input name="crj_advent_link_tag14" size="50" maxlength="50" type="text" value="'.get_option('crj_advent_link_tag14').'" />';
	$html_output .= '</div>';
	$html_output .= '</div>'.'<!-- END infotext -->'."\n";
	$html_output .= '<br/>'."\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text_title">';
	$html_output .=         'Tag 15 Beitragslink';
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .=         '<input name="crj_advent_link_tag15" size="50" maxlength="50" type="text" value="'.get_option('crj_advent_link_tag15').'" />';
	$html_output .= '</div>';
	$html_output .= '</div>'.'<!-- END infotext -->'."\n";
	$html_output .= '<br/>'."\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text_title">';
	$html_output .=         'Tag 16 Beitragslink';
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .=         '<input name="crj_advent_link_tag16" size="50" maxlength="50" type="text" value="'.get_option('crj_advent_link_tag16').'" />';
	$html_output .= '</div>';
	$html_output .= '</div>'.'<!-- END infotext -->'."\n";
	$html_output .= '<br/>'."\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text_title">';
	$html_output .=         'Tag 17 Beitragslink';
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .=         '<input name="crj_advent_link_tag17" size="50" maxlength="50" type="text" value="'.get_option('crj_advent_link_tag17').'" />';
	$html_output .= '</div>';
	$html_output .= '</div>'.'<!-- END infotext -->'."\n";
	$html_output .= '<br/>'."\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text_title">';
	$html_output .=         'Tag 18 Beitragslink';
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .=         '<input name="crj_advent_link_tag18" size="50" maxlength="50" type="text" value="'.get_option('crj_advent_link_tag18').'" />';
	$html_output .= '</div>';
	$html_output .= '</div>'.'<!-- END infotext -->'."\n";
	$html_output .= '<br/>'."\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text_title">';
	$html_output .=         'Tag 19 Beitragslink';
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .=         '<input name="crj_advent_link_tag19" size="50" maxlength="50" type="text" value="'.get_option('crj_advent_link_tag19').'" />';
	$html_output .= '</div>';
	$html_output .= '</div>'.'<!-- END infotext -->'."\n";
	$html_output .= '<br/>'."\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text_title">';
	$html_output .=         'Tag 20 Beitragslink';
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .=         '<input name="crj_advent_link_tag20" size="50" maxlength="50" type="text" value="'.get_option('crj_advent_link_tag20').'" />';
	$html_output .= '</div>';
	$html_output .= '</div>'.'<!-- END infotext -->'."\n";
	$html_output .= '<br/>'."\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text_title">';
	$html_output .=         'Tag 21 Beitragslink';
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .=         '<input name="crj_advent_link_tag21" size="50" maxlength="50" type="text" value="'.get_option('crj_advent_link_tag21').'" />';
	$html_output .= '</div>';
	$html_output .= '</div>'.'<!-- END infotext -->'."\n";
	$html_output .= '<br/>'."\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text_title">';
	$html_output .=         'Tag 22 Beitragslink';
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .=         '<input name="crj_advent_link_tag22" size="50" maxlength="50" type="text" value="'.get_option('crj_advent_link_tag22').'" />';
	$html_output .= '</div>';
	$html_output .= '</div>'.'<!-- END infotext -->'."\n";
	$html_output .= '<br/>'."\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text_title">';
	$html_output .=         'Tag 23 Beitragslink';
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .=         '<input name="crj_advent_link_tag23" size="50" maxlength="50" type="text" value="'.get_option('crj_advent_link_tag23').'" />';
	$html_output .= '</div>';
	$html_output .= '</div>'.'<!-- END infotext -->'."\n";
	$html_output .= '<br/>'."\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text_title">';
	$html_output .=         'Tag 24 Beitragslink';
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .=         '<input name="crj_advent_link_tag24" size="50" maxlength="50" type="text" value="'.get_option('crj_advent_link_tag24').'" />';
	$html_output .= '</div>';
	$html_output .= '</div>'.'<!-- END infotext -->'."\n";
	$html_output .= '<br/>'."\n";

	$html_output .= '<div id="back_link">'."\n";
	$html_output .= '<input name="submit_save" type="submit" value="Speichern">'."\n";
	$html_output .= '</div>'."\n";

	$html_output .= '</form>';
	$html_output .= '</div>'.'<!-- END Plugin_Wrapper_Content -->'."\n";
	$html_output .= '<!--end Themplate_Option_Page_Show-->'."\n"."\n";

	echo $html_output ;
	}

	} // END if ( is_admin() )

	/*
	Sourcecode von:
	http://www.smart-webentwicklung.de/2012/08/wordpress-artikel-views-anzeigen-ohne-plugin/
	echo Post_Views::increment_post_views(get_the_ID());

	//ausgabe
	echo Post_Views::get_post_views(get_the_ID());

	//Top-3 der meistgelesenen Artikel anzeigen
	echo get_popular_posts_by_views(3);

	*/
	class Post_Views
	{
	const KEY = 'post_views_count';

	private static function get_post_views_count($post_id)
	{
	return get_post_meta($post_id, self::KEY, true);
	}

	public static function get_post_views($post_id)
	{
	$count = self::get_post_views_count($post_id);
	$count = $count === '' ? 0 : $count;
	return sprintf(_n('%d Aufruf', '%d Aufrufe', $count), $count);
	}

	public static function increment_post_views($post_id)
	{
	$count = self::get_post_views_count($post_id);
	if($count === '')
	{
	delete_post_meta($post_id, self::KEY);
	add_post_meta($post_id, self::KEY, '0');
	}
	else
	{
	$count++;
	update_post_meta($post_id, self::KEY, $count);
	}
	}
	}

	function get_popular_posts_by_views($limit = 5)
	{
	global $post;

	$tmp_post = $post;

	$popular_posts = get_posts(
	array(
	'posts_per_page' => $limit,
	'meta_key' => Post_Views::KEY,
	'orderby' => Post_Views::KEY,
	'order' => 'DESC'
	)
	);

	$output = '<ul>';

	foreach($popular_posts as $tmp_post)
	{
	$title = get_the_title($tmp_post->ID);
	$output .= '<li><a href="' . get_permalink($tmp_post->ID) . '" title="Artikel: ' . $title . '">' . $title . '</a> (' . Post_Views::get_post_views($tmp_post->ID) . ')</li>';
	}

	$post = $tmp_post;

	return $output . '</ul>';
	}
?>