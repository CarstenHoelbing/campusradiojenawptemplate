<?php
/**
 * Diese PHP-Seite wird auf der Startseite und folgend aufgerufen
 * 
 * @package WordPress
 * @subpackage CampusRadioJenaWPTemplate
 * @since CampusRadioJenaWPTemplate 1.0
*/

get_header(); ?>

<div id="content_box"> <!-- BEGIN content_box -->
<div id="content_box_article"> <!-- BEGIN content_box_article -->

<?php

	$page = (get_query_var('page')) ? get_query_var('page') : 1;
	
	// ausschliessen die Kategorie 'allgemein' anzeigen 
	query_posts(array ( 'category_name' => 'allgemein','paged' => get_query_var( 'paged' )));
	global $more;
	$more = 0;
	
	while (have_posts()) : the_post(); 
	
		get_template_part( 'content_index', 'index' );
	
	endwhile;  
?>

<br/>

<?php
	//das WP Plugin 'wp_pagenavi' einfuegen (Seitennavigation)
	if (function_exists('wp_pagenavi'))
	{
		echo '<div id="pagenavi">';
		wp_pagenavi();
		echo '</div><!-- END pagenavi-->';
	}
 ?>
</div> <!-- END content_box_article -->

<?php get_sidebar(); ?>

</div> <!-- END content_box -->

<?php get_footer(); ?>