<?php
/**
 * Diese PHP-Seite wird immer am ende jeder Seite aufgerufen
 *
 * @package WordPress
 * @subpackage CampusRadioJenaWPTemplate
 * @since CampusRadioJenaWPTemplate 1.0
 */
?>

<div id="footer">

	<div id="footer_contact">
		<h6>Redaktion Campusradio Jena</h6>
		<ul>
			<li><div class="note">Ernst-Abbe-Hochschule Jena</div></li>
			<li><div class="note">Carl-Zeiss-Promenade 2</div></li>
			<li><div class="zip">07745 Jena</div></li>
			<li><div class="tel">Tel. +49 3641 205-796</div></li>
			<li><div class="email">E-Mail: redaktion@campusradio-jena.de</div></li>
		</ul>
	</div>
		
	<div id="footer_friend">
		<h6>Freunde und F&ouml;rderer des <?php bloginfo('name'); ?></h6>
		<ul>
			<li><a href="http://stura.inside-fhjena.de/" target="_blank">EAH-Stura</a></li>
			<li><a href="http://stura.uni-jena.de/" target="_blank">UNI-Stura</a></li>
			<li><a href="https://www.fsr-kowi.de/" target="_blank">FSR KoWi Jena</a></li>
			<li><a href="http://www.radio-okj.de/" target="_blank">Radio OKJ</a></li>
			<li><a href="http://www.eah-jena.de/" target="_blank">EAH-Jena</a></li>
		</ul>
	</div>


	<div id="footer_template_info">
		Copyright &copy; <?php echo date("Y") ?> <b><a href="<?php home_url(); ?>"><?php bloginfo('name'); ?></a>. All rights reserved.<br/>
		<?php echo get_template() ?> Theme by <a href="http://www.hoelbing.net/">Carsten H&ouml;lbing</a></b>.<br/>
	</div>
 
	<div id="footer_content">		
		<a href="<?php bloginfo('rss2_url'); ?>" title="RSS">RSS</a>  |
		<a href="<?php bloginfo('url'); ?>/?page_id=522" title="Impressum">Impressum</a> | 
		<a href="<?php bloginfo('url'); ?>/?page_id=12857" title="Datenschutz">Datenschutz</a> | 
		<a href="<?php bloginfo('url'); ?>/?page_id=31" title="Kontakt">Kontakt</a> | 
		<a href="<?php echo wp_login_url(); ?>" title="Login">Login</a>
	</div>

</div><!-- END  footer-->

</div><!-- END  wrapper-->

<?php
/**
 * Social Bar
 * show left, center (hor), fb img,
 */
 ?>
<div id="sidebar_socialLinks">

<a href="https://www.facebook.com/campusradiojena" target="_blank" >
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/fb_icon.small.png" alt="hier geht es zum Facebook Auftritt des Campusradio-Jena" title="hier geht es zum Facebook Auftritt des Campusradio-Jena" width="25" height="25"/>	
</a>

</div><!-- end sidebar_socialLinks -->

<?php
/**
 * Code from Piwik
 * (Hardcode)
 */
 ?>

<!-- Piwik -->
<script type="text/javascript">
	var _paq = _paq || [];
	_paq.push(["setDoNotTrack", true]);
	_paq.push(['trackPageView']);
	_paq.push(['enableLinkTracking']);
	(function() {
		var u = "//stats.stura.uni-jena.de/";
		_paq.push(['setTrackerUrl', u + 'piwik.php']);
		_paq.push(['setSiteId', 12]);
		var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
		g.type = 'text/javascript';
		g.async = true;
		g.defer = true;
		g.src = u + 'piwik.js';
		s.parentNode.insertBefore(g, s);
	})(); 
</script>
<noscript><p><img src="https://stats.stura.uni-jena.de/piwik.php?idsite=12" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->


<?php wp_footer(); ?>

</body>
</html>