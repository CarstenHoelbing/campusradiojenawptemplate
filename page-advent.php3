<?php
/**
 * (neu) geschrieben am 2015-11-26
 * von Carsten Hoelbing
 *
 * Template Name: Adventskalender
 *
 */

$year = 2015;
?>

<!--[if IE 7]>
<html class="ie ie7" lang="de-DE">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="de-DE">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="de-DE">
	<!--<![endif]-->
	<head>

		<title>Campusradio Jena - Adventskalender <?php $year ?>
		</title>

		<meta name="description" content="dies ist der Adventskalender <?php $year ?> vom Campusradio-Jena, wir wünscht allen eine besinnliche Adventszeit">
		<meta name="keywords" content="Campusradio Adventskalender, Campusradio Jena Adventskalender, Campusradio, Campusradio Jena, Jena, Adventskalender">
		<meta name="copyright" content="Campusradio Adventskalender">
		<meta name="robots" content="index,follow">

		<meta http-equiv="Language" content="de">
		<meta http-equiv="content-language" content="de-DE">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

		<meta http-equiv="Content-Style-Type" content="text/css">

		<link type="image/x-icon" rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/advent/favicon.ico">
		<link type="text/css" rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/adventskalende.index.style.css" media="screen">

		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js"></script>
		<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/javascript/jquery.snow.min.1.0.js"></script>
		<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/javascript/adventskalender2.js"></script>

		<script type="text/javascript">
			$(function() {
				$('a#tgglBox').click(function() {
					var class =
					$('#box').hasClass('visible') ? 'hidden' : 'visible';
					$.ajax({
					data : {
					display : class
					}
				});
				$('#box').toggleClass('visible');
				$('#box').toggleClass('hidden');
				return false;
			});
			});

		</script>
		<?php
		wp_head();
		?>

	</head>

	<body <?php body_class($class); ?>>

		<div id="wrapperAdvent">

			<div id="contentAdvent">
				>
				<div id="adventTree">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/advent/tree24-01.gif" width="550" height="660" border="0" alt="Baum">

					<ul>

						<li class="tag01">
							<a href="<?php echo $page_name; ?>?day_nr=01">Tag 1</a>
						</li>
						<li class="tag02">
							<a href="<?php echo $page_name; ?>?day_nr=02">Tag 2</a>
						</li>
						<li class="tag03">
							<a href="<?php echo $page_name; ?>?day_nr=03">Tag 3</a>
						</li>
						<li class="tag04">
							<a href="<?php echo $page_name; ?>?day_nr=04">Tag 4</a>
						</li>
						<li class="tag05">
							<a href="<?php echo $page_name; ?>?day_nr=05">Tag 5</a>
						</li>
						<li class="tag06">
							<a href="<?php echo $page_name; ?>?day_nr=06">Tag 6</a>
						</li>
						<li class="tag07">
							<a href="<?php echo $page_name; ?>?day_nr=07">Tag 7</a>
						</li>
						<li class="tag08">
							<a href="<?php echo $page_name; ?>?day_nr=08">Tag 8</a>
						</li>
						<li class="tag09">
							<a href="<?php echo $page_name; ?>?day_nr=09">Tag 9</a>
						</li>
						<li class="tag10">
							<a href="<?php echo $page_name; ?>?day_nr=10">Tag 10</a>
						</li>
						<li class="tag11">
							<a href="<?php echo $page_name; ?>?day_nr=11">Tag 11</a>
						</li>
						<li class="tag12">
							<a href="<?php echo $page_name; ?>?day_nr=12">Tag 12</a>
						</li>
						<li class="tag13">
							<a href="<?php echo $page_name; ?>?day_nr=13">Tag 13</a>
						</li>
						<li class="tag14">
							<a href="<?php echo $page_name; ?>?day_nr=14">Tag 14</a>
						</li>
						<li class="tag15">
							<a href="<?php echo $page_name; ?>?day_nr=15">Tag 15</a>
						</li>
						<li class="tag16">
							<a href="<?php echo $page_name; ?>?day_nr=16">Tag 16</a>
						</li>
						<li class="tag17">
							<a href="<?php echo $page_name; ?>?day_nr=17">Tag 17</a>
						</li>
						<li class="tag18">
							<a href="<?php echo $page_name; ?>?day_nr=18">Tag 18</a>
						</li>
						<li class="tag19">
							<a href="<?php echo $page_name; ?>?day_nr=19">Tag 19</a>
						</li>
						<li class="tag20">
							<a href="<?php echo $page_name; ?>?day_nr=20">Tag 20</a>
						</li>
						<li class="tag21">
							<a href="<?php echo $page_name; ?>?day_nr=21">Tag 21</a>
						</li>
						<li class="tag22">
							<a href="<?php echo $page_name; ?>?day_nr=22">Tag 22</a>
						</li>
						<li class="tag23">
							<a href="<?php echo $page_name; ?>?day_nr=23">Tag 23</a>
						</li>
						<li class="tag24">
							<a href="<?php echo $page_name; ?>?day_nr=24">Tag 24</a>
						</li>

					</ul>

				</div><!-- END adventTree -->
				</center>
				<div id="contentAdvent_info">
					<h1 id="contentAdvent_info_header">Campusradio Adventskalender</h1>
					<br/>
					<h2 id="contentAdvent_info_header_text">Das Campusradio Jena w&uuml;nscht allen eine besinnliche Adventszeit</h2>
				</div>
				<center
				</div><!-- END  contentAdvent-->

				</div><!-- END  wrapperAdvent-->

				<?php

				if (isset($_GET['day_nr']) AND ($_GET['day_nr'] >= 1) AND ($_GET['day_nr'] <= 24)) {
					$post = get_post($_GET['day_nr']);
					$pageURL = "https://" . $_SERVER['SERVER_NAME'] . "/adventskalender/";
					//hack
					if (isset($_GET['abc'])) {
						$day_nr = $_GET['day_nr'];
						$day = $_GET['day_nr'];
						$day_server = $month = 12;
					} else {
						$day_server = date('d');
						$month = date('m'); 
					}

					if (($day_server >= $day_nr) and ($month == '12')) {

						$post_id = get_option('crj_advent_link_tag' . $_GET['day_nr']);

						$queried_post = get_post($post_id);
						//$title = $queried_post ->
						post_title;

						echo '
					<div id="boxAdvent">
						' . "\n";

						echo '
						<div id="daylayer_content">
							' . "\n";

						echo '
							<div id="daylayer_content_header">
								';
						echo htmlentities($queried_post -> post_title);
						echo '
							</div>' . "\n";

						// textausgabe
						echo '
							<div id="daylayer_content_header_text">
								' . "\n";
						$content = $queried_post -> post_content;
						$content = apply_filters('the_content', $content);
						$content = str_replace(']]>', ']]&gt;', $content);
						echo $content . "\n";
						echo '
							</div>' . "\n";

						echo '
						</div><!-- END  daylayer_content-->' . "\n";

						echo '
						<div id="daylayer_footer">
							' . "\n";
						echo '<img src="/wp-content/themes/CampusRadioJenaWPTemplate/images/advent/zweig.gif" width="60" height="37" border="0" alt="zweig">' . "\n";
						echo '
							<br>
							<br>
							' . "\n";
						echo '<a id="tgglBox" href="' . $pageURL . '" >Fenster schlie&szlig;en!</a>' . "\n";
						echo '
						</div><!-- END  daylayer_footer-->' . "\n" . "\n";

						echo '
					</div><!-- END  boxAdvent-->' . "\n";

					} else if ($month != '12') {
						echo '
					<div id="boxAdvent_error">
						';

						echo '
						<div id="daylayer_content">
							' . "\n";

						echo '
							<div id="daylayer_content_header">
								';
						echo 'ja ist denn schon Dezember oder was?';
						echo '
							</div>' . "\n";

						echo '
							<center>
								' . "\n";
						echo '
								<div id="daylayer_content_player">
									';
						echo '<img src="/wp-content/themes/CampusRadioJenaWPTemplate/images/advent/w-mann01-ani.gif" width="65" height="103" border="0" alt="bad" align="middle">';
						echo '
								</div>' . "\n";

						// textausgabe
						echo '
								<div id="daylayer_content_header_text">
									' . "\n";
						echo 'Mogeln gilt nicht';
						echo '
								</div>
								' . "\n";
						echo '
								<center>
									' . "\n";

						echo '
						</div><!-- END  daylayer_content-->' . "\n";

						echo '
						<div id="daylayer_footer">
							' . "\n";
						echo '<img src="/wp-content/themes/CampusRadioJenaWPTemplate/images/advent/zweig.gif" width="60" height="37" border="0" alt="zweig">' . "\n";
						echo '
							<br>
							<br>
							' . "\n";
						echo '<a id="tgglBox" href="' . $pageURL . '" >Fenster schlie&szlig;en!</a>' . "\n";
						echo '
						</div><!-- END  daylayer_footer-->' . "\n" . "\n";

						echo '
					</div><!-- END  boxAdvent-->' . "\n";

					} else {
						echo '
					<div id="boxAdvent_error">
						';

						echo '
						<div id="daylayer_content">
							' . "\n";

						echo '
							<div id="daylayer_content_header">
								';
						echo 'Böse Kinder kriegen nix!';
						echo '
							</div>
							' . "\n";
						echo '
							<center>
								' . "\n";
						echo '
								<div id="daylayer_content_player">
									';
						echo '<img src="/wp-content/themes/CampusRadioJenaWPTemplate/images/advent/w-mann01-ani.gif" width="65" height="103" border="0" alt="bad" align="middle">';
						echo '
								</div>' . "\n";

						// textausgabe
						echo '
								<div id="daylayer_content_header_text">
									' . "\n";
						echo 'Mogeln gilt nicht, nur die Türchen für den heutigen oder einen früheren Tag öffnen. ';
						echo '
								</div>
								' . "\n";
						echo '
							</center>' . "\n";

						echo '
						</div><!-- END  daylayer_content-->' . "\n";

						echo '
						<div id="daylayer_footer">
							' . "\n";
						echo '<img src="/wp-content/themes/CampusRadioJenaWPTemplate/images/advent/zweig.gif" width="60" height="37" border="0" alt="zweig" align="middle">' . "\n";
						echo '
							<br>
							<br>
							' . "\n";
						echo '<a id="tgglBox" href="' . $pageURL . '" >Fenster schlie&szlig;en!</a>' . "\n";
						echo '
						</div><!-- END  daylayer_footer-->' . "\n" . "\n";

						echo '
					</div><!-- END  boxAdvent-->' . "\n";
					}
				}
					?>

					<div id="footer">
						<script type="text/javascript">
							$(document).ready(function() {

								$.fn.snow();

							});
						</script>

						<?php
						$html_output = "";
						$time_now = mktime(date("H"), date("i"), date("s"), date("n"), date("j"));
						$time_adv = mktime("0", "0", "0", "12", "25");
						$time_div = $time_adv - $time_now;
						$time_div = $time_div / 60 / 60 / 24;
						$html_output = "heute ist " . $TageLang[date("w")] . " der " . date("d") . ". " . $MonateLang[date("n") - 1] . " " . date("Y");

						if ($month == '12') {
							//$day = date("j");
							if ($day <= 22) {
								$html_output .= " - noch " . sprintf("%d", $time_div + 1) . " Tage bis Weihnachten ";
								$html_output .= "- Sucht das Fensterchen f&uuml;r heute!";
							} else if ($day == 23) {
								$html_output .= "Morgen ist Heiligabend!";
							} else if ($day == 24) {
								$html_output .= "Heut ist Heiligabend!";
							} else if (($day >= 25) and ($day <= 30)) {
								$html_output .= "Frohe Weihnachten - Alle Fensterchen d&uuml;rfen ge&ouml;ffnet werden";
							} else {
								$html_output .= "Weihnachten ist vorbei!";
							}
						} else {
							$html_output .= " - noch " . sprintf("%d", $time_div) . " Tage bis Weihnachten";
						}

						echo $html_output;
						?>
						<br/>
						<a href="www.campusradio-jena.de" >www.campusradio-jena.de</a>
					</div><!-- END  footer-->

			</div><!-- END  wrapper-->

			<!-- Piwik -->
			<script type="text/javascript">
				var _paq = _paq || [];
				_paq.push(["trackPageView"]);
				_paq.push(["enableLinkTracking"]);

				(function() {
					var u = (("https:" == document.location.protocol) ? "https" : "http") + "://stats.stura.uni-jena.de/";
					_paq.push(["setTrackerUrl", u + "piwik.php"]);
					_paq.push(["setSiteId", "12"]);
					var d = document, g = d.createElement("script"), s = d.getElementsByTagName("script")[0];
					g.type = "text/javascript";
					g.defer = true;
					g.async = true;
					g.src = u + "piwik.js";
					s.parentNode.insertBefore(g, s);
				})();
			</script>
			<!-- End Piwik Code -->

			<?php wp_footer(); ?>

	</body>

</html>