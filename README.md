# Readme

This is a Wordpress-Template for CampusRadio-Jena.
CampusRadio-Jena is a german student radio.

Campusradio Jena 103,4 ist ein Projekt der beiden Jenaer Studentenräte und sendet ein tägliches 
Fensterprogramm im Jenaer Stadtgebiet auf UKW 103.4,im Kabel auf 107.9 und im Internetstream.

## Metadata

- Theme Name: CampusRadioJenaWPTemplate
- Author: Carsten Hölbing
- Release: 2012
- Version: 1.3
- License: MIT license
- License URI: http://opensource.org/licenses/mit-license.php
- Tags: best Wordpress-Template for CampusRadio-Jena