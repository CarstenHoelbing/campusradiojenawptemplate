<?php
/**
 * Diese PHP-Seite wird aufgerufen wenn eine 
 * Sidebar angezeigt werden soll.
 * 
 * @package WordPress
 * @subpackage CampusRadioJenaWPTemplate
 * @since CampusRadioJenaWPTemplate 1.0
*/
?>

<div id="box_sidebar">
<?php

	dynamic_sidebar('sidebar_box_content_banner');
	
	if (function_exists('WPWall_Widget'))
	{
		 WPWall_Widget();
	}

	dynamic_sidebar('sidebar_box_content');

?>

<div id="box_sidebar_popular_posts">
	<?php //get_popular_posts_by_views(5); ?>
</div>

</div><!-- end sidebar -->