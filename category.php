<?php
/**
 * Diese PHP-Seite wird auf der Startseite und folgend aufgerufen
 * 
 * @package WordPress
 * @subpackage CampusRadioJenaWPTemplate
 * @since CampusRadioJenaWPTemplate 1.0
*/
?>

<?php get_header(); ?>

<div id="content_box"> <!-- BEGIN content_box -->
<div id="content_box_article"> <!-- BEGIN content_box_article -->
<div id="content_box_info_header"><?php echo "Kategorie: ".get_query_var( 'category_name' ); ?></div>

<?php 

query_posts(array ( 
'category_name' => get_query_var( 'category_name' ),
'paged' => get_query_var( 'paged' ),
'posts_per_page' => 15
));
?>

<?php

while (have_posts()) : the_post(); 

?>

<div class="article"><!-- START article -->

	<?php 	the_time('j F, Y');?>  <a href="<?php the_permalink() ?>"><?php trim(the_title())?></a>

</div><!-- END article -->

<?php endwhile;  ?>
<br/>
<div id="pagenavi">
	<?php wp_pagenavi(); ?>
</div><!-- END pagenavi-->

</div> <!-- END content_box_article -->

<?php get_sidebar(); ?>

</div> <!-- END content_box -->

<?php get_footer(); ?>

?>