<?php
/**
 * Diese PHP-Seite wird aufgerufen wenn eine 
 * Seite/Page angezeigt werden soll.
 * 
 * @package WordPress
 * @subpackage CampusRadioJenaWPTemplate
 * @since CampusRadioJenaWPTemplate 1.0
*/
?>

<?php get_header(); ?>

<div id="content_box"> <!-- BEGIN content_box -->
<div id="content_box_article"> <!-- BEGIN content_box_article -->

<?php

global $more;
$more = 0;
?>
 
<?php while (have_posts()) : the_post();

	get_template_part( 'content_page', 'page' );


endwhile;  ?>

</div> <!-- END content_box_article -->

</div> <!-- END content_box -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>