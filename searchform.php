<?php
/**
 * searchform
 * 
 * @package WordPress
 * @subpackage CampusRadioJenaWPTemplate
 * @since CampusRadioJenaWPTemplate 1.0
*/
?>

<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
<p>
		<label for="s" class="assistive-text">Suchen nach: </label>
		<input type="text" class="field" name="s" id="s"  value="..." onfocus="if (this.value == '...') {this.value = '';}" onblur="if (this.value == '') {this.value = '...';}" />
		<input type="submit" class="submit" name="submit" id="searchsubmit" value="Suchen" />
</p>
</form>